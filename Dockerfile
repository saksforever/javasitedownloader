FROM maven:3.9.1-eclipse-temurin-20 AS build
COPY src /home/app/src
COPY pom.xml /home/app
RUN mvn -f /home/app/pom.xml clean package

#
# Package stage
#
FROM maven:3.9.1-eclipse-temurin-20
COPY --from=build /home/app/target/downloader-jar-with-dependencies.jar /usr/local/lib/downloader.jar
ENTRYPOINT ["java","-jar","/usr/local/lib/downloader.jar"]