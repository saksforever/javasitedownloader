package com.example;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DownloadTaskTest {

    private final URL url = new URL("http://education.simcat.ru/uo/img/1222420284_gerb_dlya_sayta_1.jpg");

    String currentPath = Paths.get("").toAbsolutePath() + FileSystems.getDefault().getSeparator();
    String filePath = currentPath + "1222420284_gerb_dlya_sayta_1.jpg";

    File file = new File(filePath);

    DownloadTaskTest() throws MalformedURLException {
    }

    @Test
    void downloadFile() throws Exception {
        DownloadTask.DownloadFile(url, currentPath);
        assertEquals(FileUtils.sizeOf(file), 30672);
        Files.deleteIfExists(Paths.get(filePath));
    }

}