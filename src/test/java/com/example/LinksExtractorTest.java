package com.example;

import java.io.IOException;
import java.net.URL;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class LinksExtractorTest {

    private final String url = "http://education.simcat.ru/";
    private List<URL> list;

    @org.junit.jupiter.api.Test
    void extract() throws IOException {
        list = LinksExtractor.extract(url);
        assertEquals(list.size(), 15);
    }

}