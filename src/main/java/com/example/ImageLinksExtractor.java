package com.example;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CopyOnWriteArrayList;

public class ImageLinksExtractor implements Callable<CopyOnWriteArrayList<URL>> {

    private final String url;

    public ImageLinksExtractor(String url) {
        this.url = url;
    }

    public static CopyOnWriteArrayList<URL> extract(String url) throws IOException {
        final CopyOnWriteArrayList<URL> result = new CopyOnWriteArrayList<URL>();
        try {
            Document doc = Jsoup.connect(url).ignoreContentType(true).ignoreHttpErrors(true).get();
            Elements media = doc.select("img");
            for (Element src : media) {
                try {
                    result.add(new URL(src.attr("abs:src")));
                } catch (MalformedURLException e) {
                    try {
                        result.add(new URL(src.attr("abs:data-src")));
                    } catch (MalformedURLException ee) {
                        System.out.println(src + " malformed");
                    }
                }
            }
        } catch (SocketTimeoutException e) {
            System.out.println(url + " SocketTimeoutException");
        } catch (Exception ee) {
        }
        return result;
    }

    public CopyOnWriteArrayList<URL> call() {
        try {
            return extract(url);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}