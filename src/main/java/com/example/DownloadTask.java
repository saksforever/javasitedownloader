package com.example;

import org.apache.commons.io.FilenameUtils;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.Random;

public class DownloadTask implements Runnable {

    private final URL url;
    private final String Path;

    public DownloadTask(URL url, String Path) {
        this.url = url;
        this.Path = Path;
    }

    public static void DownloadFile(URL url, String Path) throws Exception {
        String name = FilenameUtils.getName(url.getPath());

        int timeout = 3000;
        long minDownloadSize;

        if (!Objects.equals(System.getenv("MIN_SIZE_KB"), null)) {
            minDownloadSize = Long.parseLong(System.getenv("MIN_SIZE_KB")) * 1024;
        } else {
            minDownloadSize = 30000;
        }

        try {
            URLConnection conn = url.openConnection();
            conn.setConnectTimeout(timeout);
            if (conn.getContentLength() > minDownloadSize) {
                InputStream in = conn.getInputStream();
                System.out.println(Path + name);
                Files.copy(in, Paths.get(Path + name));
                System.out.println(Path + name + " write end");
                in.close();
            }
        } catch (FileAlreadyExistsException e) {
            System.out.println("file " + name + " exists");
            Random random = new Random();
            String NewName = random.ints(97, 123)// random 5 symbol word
                    .limit(5)
                    .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                    .toString();
            URLConnection conn = url.openConnection();
            InputStream in = conn.getInputStream();
            System.out.println(Path + NewName + name);
            Files.copy(in, Paths.get(Path + NewName + name));
            System.out.println(Path + NewName + name + " write end");
            in.close();
        } catch (Exception e) {
            e.getMessage();
        }
    }

    public void run() {
        try {
            DownloadFile(url, Path);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
