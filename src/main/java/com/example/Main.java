package com.example;

import java.net.URL;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.*;

class Main {

    public static void main(String[] args) throws Exception {
        long startTime = System.nanoTime();
        String dir;

        if (!Objects.equals(System.getenv("DOWNLOADS_DIR"), null)) {
            dir = System.getenv("DOWNLOADS_DIR");
        } else {
            dir = "downloads";
        }

        String currentDir = Paths.get("").toAbsolutePath() + FileSystems.getDefault().getSeparator();
        String downloadToDir = currentDir + dir + FileSystems.getDefault().getSeparator(); // downloadToDir

        String downloadFromUrl;

        if (!Objects.equals(System.getenv("URL"), null)) {
            downloadFromUrl = System.getenv("URL");
        } else {
            downloadFromUrl = "https://www.spbstu.ru/";
        }

        try {
            Files.createDirectory(Paths.get(downloadToDir));
        } catch (FileAlreadyExistsException e) {
            System.out.println("dir " + downloadToDir + " already exists");
        }

        CopyOnWriteArrayList<URL> imageLinks = ImageLinksExtractor.extract(downloadFromUrl);
        System.out.println(imageLinks.size() + " images urls were found on main page");

        List<URL> links = LinksExtractor.extract(downloadFromUrl);
        System.out.println(links.size() + " inners urls were found");

        // Start getting images links
        System.out.println("Start getting images links...");
        ExecutorService threadPool = Executors.newFixedThreadPool(10);
        List<Future<CopyOnWriteArrayList<URL>>> features = new ArrayList<>();
        for (URL link : links) {
            try {
                features.add(threadPool.submit(new ImageLinksExtractor(link.toString())));
            } catch (Exception ex) {
                System.out.println("error in link: " + link);
            }
        }
        while (!threadPool.isTerminated()) {
            threadPool.shutdown();
            Thread.sleep(10000);
            System.out.println("Getting images links in progress. Please wait");
        }

        // collect all images links
        for (Future<CopyOnWriteArrayList<URL>> future : features) {
            imageLinks.addAll(future.get());
        }

        // Start downloading images
        System.out.println("Start downloading images...");
        threadPool = Executors.newFixedThreadPool(40);
        for (URL link : imageLinks) {
            System.out.println("link " + link);
            threadPool.submit(new DownloadTask(link, downloadToDir));
        }

        while (!threadPool.isTerminated()) {
            threadPool.shutdown();
            Thread.sleep(10000);
            System.out.println("Downloading images links in progress. Please wait");
        }

        long endTime = System.nanoTime();
        long totalTime = endTime - startTime;
        System.out.println("Total time nanoSec: " + totalTime);
    }
}
