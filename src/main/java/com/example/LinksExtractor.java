package com.example;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class LinksExtractor implements Callable<List<URL>> {

    private final String url;

    public LinksExtractor(String url) {
        this.url = url;
    }

    public static List<URL> extract(String url) throws IOException {
        final ArrayList<URL> result = new ArrayList<>();

        try {
            Document doc = Jsoup.connect(url).ignoreHttpErrors(true).get();
            Elements links = doc.select("a[href^=http]");
            for (Element link : links) {
                result.add(new URL(link.attr("abs:href")));
            }
        } catch (ConnectException ce) {
            System.out.println(url + " Connection Exception");
        } catch (SocketException se) {
            System.out.println(url + " Socket Exception");
        } catch (Exception eee) {
            System.out.println(url + "  some exception");
            System.out.println(eee.getMessage());
        }
        return result;
    }

    public List<URL> call() {
        try {
            return extract(url);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
